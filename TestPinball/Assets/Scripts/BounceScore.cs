﻿using UnityEngine;

public class BounceScore : MonoBehaviour {

	public Color collideColor;
	public float collideFlickDuration = 0.1f;
	[Space(6)]
	public int scores;

	private MeshRenderer meshRenderer;
	private Color originalColor;

	private void Awake() {
		Initialize();
	}

	private void Initialize() {
		meshRenderer = GetComponentInChildren<MeshRenderer>();
		originalColor = meshRenderer.material.GetColor("_Color");
	}

	private void OnCollisionEnter(Collision collision) {
		Ball ball = collision.transform.GetComponent<Ball>();
		if (ball != null) {
			ReceiveScores();
			Flick();
		}
	}

	private void ReceiveScores() {
		Scores.instance.Add(scores);
	}

	private void Flick() {
		SetColor(collideColor);
		Invoke("ResetColor", collideFlickDuration);
	}

	private void SetColor(Color color) {
		meshRenderer.material.SetColor("_Color", color);
	}

	private void ResetColor() {
		SetColor(originalColor);
	}
}
