﻿using UnityEngine;

public class Bounce : MonoBehaviour {

	public float bounceForce = 1.3f;

	private void OnCollisionEnter(Collision collision) {
		Ball ball = collision.transform.GetComponent<Ball>();

		if (ball != null)
			ball.rb.AddForce(-collision.contacts[0].normal * bounceForce, ForceMode.VelocityChange);
	}
}
