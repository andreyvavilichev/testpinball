﻿using UnityEngine;

public class DeactivateAfterTime : MonoBehaviour {

	public float lifeTime;
	[Space(6)]
	public bool destroy;

	private void OnEnable() {
		Invoke("Deactivate", lifeTime);
	}

	private void Deactivate() {
		if (destroy)
			Destroy(gameObject);
		else
			gameObject.SetActive(false);
	}

	private void OnDisable() {
		CancelInvoke();
	}
}
