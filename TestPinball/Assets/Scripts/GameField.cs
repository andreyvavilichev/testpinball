﻿using UnityEngine;

public class GameField : MonoBehaviour {

	public Transform spawnPoint;
	public Ball ballPref;
	[Space(6)]
	public float maxForcePower = 22f;
	public float minForcePower = 6f;

	public void Shot(float shotPowerNormalized) {
		float power = Mathf.Lerp(minForcePower, maxForcePower, shotPowerNormalized);
		ShootBall(power);
	}

	private void ShootBall(float force) {
		Ball ball = Instantiate(ballPref, spawnPoint.position, spawnPoint.rotation);
		ball.rb.AddForce(spawnPoint.forward * force, ForceMode.VelocityChange);
		CustomInput.instance.Activate();
	}
}
