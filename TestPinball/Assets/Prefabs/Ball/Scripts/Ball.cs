﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Ball : MonoBehaviour {

	public static Ball instance { get; private set; }
	public static bool isInitialized { get { return instance != null; } }

	public Rigidbody rb;

	private void Awake() {
		CreateSingleton();
		Initialize();
	}

	private void CreateSingleton() {
		instance = this;
	}

	private void Initialize() {
		rb = GetComponent<Rigidbody>();
	}
}
