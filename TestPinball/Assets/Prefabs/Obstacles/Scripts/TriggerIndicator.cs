﻿using UnityEngine;

public class TriggerIndicator : SpriteIndicator {
	private void OnTriggerEnter(Collider other) {
		Ball ball = other.GetComponentInParent<Ball>();
		if (ball != null)
			Activate();
	}
}
