﻿using System.Collections;
using UnityEngine;

public class IndicatorGroup : MonoBehaviour {

	public int scores = 60;

	protected Indicator[] indicators;

	private void Start() {
		Initialize();
		StartLive();
	}

	private void Initialize() {
		indicators = GetComponentsInChildren<Indicator>();
	}

	private void StartLive() {
		StartCoroutine("LiveAsync");
	}

	private IEnumerator LiveAsync() {
		WaitForSeconds frame = new WaitForSeconds(0.2f);
		while (true) {
			yield return frame;
			if (AllIndicatorsActivated())
				yield return StartCoroutine("AddScoresAsync");
		}
	}

	private bool AllIndicatorsActivated() {
		foreach (Indicator indicator in indicators) {
			if (!indicator.isActivated)
				return false;
		}
		return true;
	}

	private IEnumerator AddScoresAsync() {
		Scores.instance.Add(scores);
		for (int i = 0; i < 3; i++) {
			DeactivateAll();
			yield return new WaitForSeconds(0.2f);
			ActivateAll();
			yield return new WaitForSeconds(0.2f);
		}
		DeactivateAll();
	}

	private void DeactivateAll() {
		foreach (Indicator indicator in indicators)
			indicator.Deactivate();
	}

	private void ActivateAll() {
		foreach (Indicator indicator in indicators)
			indicator.Activate();
	}
}
