﻿using UnityEngine;

public abstract class Indicator : MonoBehaviour {

	public bool isActivated { get { return IsActivated(); } }

	public Color defaultColor = Color.white;
	public Color activatedColor = Color.green;

	protected abstract bool IsActivated();

	protected virtual void Awake() {
		Initialize();
	}

	protected abstract void Initialize();

	public abstract void Activate();
	public abstract void Deactivate();
}
