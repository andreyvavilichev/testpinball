﻿using UnityEngine;

public class MeshIndicator : Indicator {

	protected MeshRenderer meshRender;

	protected override void Initialize() {
		meshRender = GetComponentInChildren<MeshRenderer>();
	}

	public override void Activate() {
		SetColor(activatedColor);
	}

	private void SetColor(Color color) {
		meshRender.material.SetColor("_Color", color);
	}

	public override void Deactivate() {
		SetColor(defaultColor);
	}

	protected override bool IsActivated() {
		return meshRender.material.GetColor("_Color") == activatedColor;
	}
}
