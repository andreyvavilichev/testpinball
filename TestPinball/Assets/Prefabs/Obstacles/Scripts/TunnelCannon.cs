﻿using System.Collections;
using UnityEngine;

public class TunnelCannon : MonoBehaviour {

	public Transform snapPoint;
	[Space(6)]
	public float delay = 1.5f;
	public float shotPower = 22f;
	[Space(6)]
	public int scores;

	private void OnTriggerEnter(Collider other) {
		Ball ball = other.GetComponentInParent<Ball>();
		if (ball != null)
			StartShooting(ball);
	}

	private void StartShooting(Ball ball) {
		Scores.instance.Add(scores);
		SnapBall(ball);
		StartCoroutine("ShotAsync", ball);
	}

	private void SnapBall(Ball ball) {
		ball.transform.position = snapPoint.position;
		ball.transform.rotation = snapPoint.rotation;
		ball.rb.isKinematic = true;
	}

	private IEnumerator ShotAsync(Ball ball) {
		yield return new WaitForSeconds(delay);
		ShotBall(ball);
	}

	private void ShotBall(Ball ball) {
		ball.rb.isKinematic = false;
		ball.rb.AddForce(snapPoint.forward * shotPower, ForceMode.VelocityChange);
	}
}
