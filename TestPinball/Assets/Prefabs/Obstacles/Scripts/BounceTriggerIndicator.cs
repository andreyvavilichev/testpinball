﻿using UnityEngine;

public class BounceTriggerIndicator : MeshIndicator {
	private void OnCollisionEnter(Collision collision) {
		Ball ball = collision.transform.GetComponent<Ball>();

		if (ball != null)
			Activate();
	}
}
