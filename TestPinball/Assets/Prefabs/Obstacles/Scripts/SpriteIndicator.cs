﻿using UnityEngine;

public class SpriteIndicator : Indicator {

	protected SpriteRenderer spriteRenderer;

	protected override void Initialize() {
		spriteRenderer = GetComponentInChildren<SpriteRenderer>();
	}

	public override void Activate() {
		spriteRenderer.color = activatedColor;
	}

	public override void Deactivate() {
		spriteRenderer.color = defaultColor;
	}

	protected override bool IsActivated() {
		return spriteRenderer.color == activatedColor;
	}
}
