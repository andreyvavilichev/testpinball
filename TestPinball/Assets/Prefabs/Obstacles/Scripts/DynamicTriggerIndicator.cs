﻿using UnityEngine;

public class DynamicTriggerIndicator : SpriteIndicator {

	private void OnTriggerEnter(Collider other) {
		Ball ball = other.GetComponentInParent<Ball>();
		if (ball != null)
			TryToActivate();
	}

	private void TryToActivate() {
		if (isActivated)
			Deactivate();
		else
			Activate();
	}
}
