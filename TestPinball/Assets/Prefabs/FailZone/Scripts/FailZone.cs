﻿using UnityEngine;

public class FailZone : MonoBehaviour {

	private void OnTriggerEnter(Collider other) {
		Ball ball = other.gameObject.GetComponentInParent<Ball>();
		if (ball != null)
			Fail(ball);
	}

	private void Fail(Ball ball) {
		Destroy(ball.gameObject);
		CustomInput.instance.Deactivate();
		GameManager.instance.LostTheBall();
	}
}
