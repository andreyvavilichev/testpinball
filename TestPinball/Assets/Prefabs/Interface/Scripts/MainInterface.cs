﻿using UnityEngine;

public class MainInterface : MonoBehaviour {

	public UIScreen[] screens;

	public UIScreen defaultActiveScreen;

	public void Start() {
		CreateScreens();
	}

	private void CreateScreens() {
		foreach(UIScreen screen in screens) {
			UIScreen createdScreen = Instantiate(screen, transform);
			createdScreen.Initialize();
			if (defaultActiveScreen != null && defaultActiveScreen == screen)
				createdScreen.Show();
		}
	}

	public void OnMenuBtnClick() {
		GameManager.instance.StopPlaying();
		MainMenuScreen.instance.Show();
	}
}
