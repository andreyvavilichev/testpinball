﻿using UnityEngine;

public abstract class UIScreen : MonoBehaviour {

	public Animator animator;

	private const string TRIGGER_SHOW = "show";
	private const string TRIGGER_HIDE = "hide";
	private const string TRIGGER_SHOW_INSTANT = "show_instant";

	public abstract void Initialize();


	public virtual void Show() {
		gameObject.SetActive(true);
		SetTrigger(TRIGGER_SHOW);
	}

	private void SetTrigger(string triggerName) {
		animator.SetTrigger(triggerName);
	}

	public void ShowInstant() {
		SetTrigger(TRIGGER_SHOW_INSTANT);
	}

	public virtual void Hide() {
		SetTrigger(TRIGGER_HIDE);
	}
	public virtual void HideInstant() {
		gameObject.SetActive(false);
	}
}
