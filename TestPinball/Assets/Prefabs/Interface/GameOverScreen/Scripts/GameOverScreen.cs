﻿using UnityEngine;
using UnityEngine.UI;

public class GameOverScreen : UIScreen {

	public static GameOverScreen instance { get; private set; }

	[Space(6)]
	public Text scoresField;
	public Text newRecordField;

	public override void Initialize() {
		CreateSingleton();
	}

	private void CreateSingleton() {
		instance = this;
	}

	public override void Show() {
		scoresField.text = "RESULT\n" + Scores.totalScores.ToString();
		newRecordField.gameObject.SetActive(Scores.newHighScore);
		base.Show();
	}

	public void OnMenuBtnClick() {
		Hide();
		MainMenuScreen.instance.Show();
	}

	public void OnRepeatBtnClick() {
		Hide();
		GameManager.instance.PlayGame();
	}

	
}
