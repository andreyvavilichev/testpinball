﻿using UnityEngine;
using UnityEngine.UI;

public class PowerIndicator : MonoBehaviour {

	public static PowerIndicator instance { get; private set; }

	public Image indicatorIMG;

	private void Awake() {
		CreateSingleton();
	}

	private void CreateSingleton() {
		instance = this;
	}

	public void SetPowerValue(float valueNormalized) {
		indicatorIMG.fillAmount = Mathf.Clamp(valueNormalized, 0f, 1f);
	}
}
