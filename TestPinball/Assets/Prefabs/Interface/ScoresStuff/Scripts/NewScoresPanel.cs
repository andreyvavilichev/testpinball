﻿using UnityEngine;

public class NewScoresPanel : MonoBehaviour {

	public static NewScoresPanel instance { get; private set; }
	public static bool isInitialized { get { return instance != null; } }

	public NewScores newScoresPref;

	private void Awake() {
		CreateSingleton();
	}

	private void CreateSingleton() {
		instance = this;
	}

	public void Show(int scores) {
		NewScores createdObject = Instantiate(newScoresPref, transform);
		createdObject.SetValue(scores);
	}
}
