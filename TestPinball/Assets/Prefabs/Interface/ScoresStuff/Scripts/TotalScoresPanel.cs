﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TotalScoresPanel : MonoBehaviour {

	public Text scoresField;

	private void OnEnable() {
		StartCoroutine("ObserveAsync");
	}

	private IEnumerator ObserveAsync() {
		WaitForSeconds frame = new WaitForSeconds(0.2f);
		while(true) {
			yield return frame;
			scoresField.text = Scores.totalScores.ToString();
		}
	}

	private void OnDisable() {
		StopAllCoroutines();
	}
}
