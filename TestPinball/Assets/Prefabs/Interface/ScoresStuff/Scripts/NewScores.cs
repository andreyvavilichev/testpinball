﻿using UnityEngine;
using UnityEngine.UI;

public class NewScores : MonoBehaviour {

	public Text valueField;

	public void SetValue(int value) {
		valueField.text = value.ToString();
	}
}
