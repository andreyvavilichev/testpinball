﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class MainMenuScreen : UIScreen {
	public static MainMenuScreen instance { get; private set; }

	public override void Initialize() {
		CreateSingleton();
	}

	private void CreateSingleton() {
		instance = this;
	}

	public void OnStartManualBtnClick() {
		GameManager.instance.PlayManual();
		Hide();
	}

	public void OnStartAIBtnClick() {
		GameManager.instance.PlayAI();
		Hide();
	}

	public void OnExitGameBtnClick() {
#if UNITY_EDITOR
		EditorApplication.isPlaying = false;
#endif
		Application.Quit();
	}
}
