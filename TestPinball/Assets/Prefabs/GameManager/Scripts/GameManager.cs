﻿using UnityEngine;

public enum GameState {
	Menu,
	GamePlay
}

public enum GameMode {
	Manual,
	AI
}

public class GameManager : MonoBehaviour {

	public static GameManager instance { get; private set; }
	public static GameState gameState { get; private set; }
	public static bool canControl { get { return gameState.Equals(GameState.GamePlay); } }
	public static GameMode gameMode { get; private set; }
	public static bool isManualMode { get { return gameMode.Equals(GameMode.Manual); } }

	public GameField gameField;
	public int ballsForGameOver = 3;

	public float shotPowerNormalized { get; private set; }

	private int ballsLost;

	public const float SHOT_POWER_STEP = 0.01f;


	private void Awake() {
		CreateSingleton();
		Application.targetFrameRate = 60;
	}

	private void CreateSingleton() {
		instance = this;
	}

	private void Update() {
		if (!Ball.isInitialized) {
			if (CustomInput.holdForShoot)
				IncreasePower();
			TryToShot();
		}
		UpdateIndicatorValue();
	}

	public void IncreasePower() {
		shotPowerNormalized = Mathf.Clamp(shotPowerNormalized + SHOT_POWER_STEP, 0f, 1f);
	}

	private void TryToShot() {
		if (CustomInput.shot)
			Shot();
	}

	public void Shot() {
		gameField.Shot(shotPowerNormalized);
		shotPowerNormalized = 0;
	}
	
	private void UpdateIndicatorValue() {
		PowerIndicator.instance.SetPowerValue(shotPowerNormalized);
	}

	public void PlayManual() {
		gameMode = GameMode.Manual;
		PlayGame();
	}

	public void PlayGame() {
		Scores.instance.Reset();
		gameState = GameState.GamePlay;
		ballsLost = 0;
	}

	public void PlayAI() {
		gameMode = GameMode.AI;
		PlayGame();
	}

	public void StopPlaying() {
		gameState = GameState.Menu;
		if (Ball.isInitialized)
			Destroy(Ball.instance.gameObject);
	}

	public void LostTheBall() {
		ballsLost++;
		if (ballsLost >= ballsForGameOver) {
			StopPlaying();
			GameOverScreen.instance.Show();
		}
	}
}
