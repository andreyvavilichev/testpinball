﻿using System.Collections;
using UnityEngine;

public class AIController : MonoBehaviour {

	private bool isIncreasingPower;

	private void Update() {
		if (NeedToStartIncreasingPower())
			StartIncreasingPower();
	}

	private bool NeedToStartIncreasingPower() {
		return !GameManager.isManualMode && GameManager.canControl && !Ball.isInitialized && !isIncreasingPower;
	}

	private void StartIncreasingPower() {
		isIncreasingPower = true;
		StartCoroutine("IncreasingPowerAsync");
	}

	private IEnumerator IncreasingPowerAsync() {
		float rPower = Random.Range(0f, 1f);
		while(rPower > 0f) {
			rPower -= GameManager.SHOT_POWER_STEP;
			GameManager.instance.IncreasePower();
			yield return null;
		}
		GameManager.instance.Shot();
		isIncreasingPower = false;
	}
}
