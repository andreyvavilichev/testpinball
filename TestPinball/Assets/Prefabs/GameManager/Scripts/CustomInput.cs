﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CustomInput : MonoBehaviour {

	public static CustomInput instance { get; private set; }

	public bool isActiveControl { get; private set; }

	public static bool holdForShoot { get; private set; }
	public static bool shot { get; private set; }
	public static bool holdLeftHandle { get; private set; }
	public static bool holdRightHandle { get; private set; }

	private void Awake() {
		CreateSingleton();
	}

	private void CreateSingleton() {
		instance = this;
	}



#if UNITY_ANDROID// && !UNITY_EDITOR
	private void Update() {
		if (GameManager.canControl)
		{
			shot = false;
			holdLeftHandle = false;
			holdRightHandle = false;

			foreach (Touch touch in Input.touches) {
				if (!IsPointerOverUIObject(touch)) {
					if (!Ball.isInitialized)
						CheckHold(touch);
					else
						CheckControl(touch);
				}
			}
		}
	}

	private bool IsPointerOverUIObject(Touch touch) {
		PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
		eventDataCurrentPosition.position = touch.position;
		List<RaycastResult> results = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
		return results.Count > 0;
	}

	private void CheckHold(Touch touch) {
		if (touch.phase == TouchPhase.Began)
			holdForShoot = true;
		else if (touch.phase == TouchPhase.Ended) {
			holdForShoot = false;
			shot = true;
		}
	}

	private void CheckControl(Touch touch) {
		

		if (touch.position.x < Screen.width / 2f)
			holdLeftHandle = true;
		else
			holdRightHandle = true;
	}

#else
	private void Update() {
		if (GameManager.canControl) {
			shot = false;
			holdForShoot = false;
			shot = false;
			if (!EventSystem.current.IsPointerOverGameObject()) {
				holdForShoot = Input.GetMouseButton(0);
				shot = Input.GetMouseButtonUp(0);
				holdLeftHandle = Input.GetKey(KeyCode.A);
				holdRightHandle = Input.GetKey(KeyCode.D);
			}
		}
	}

#endif
	public void Activate() {
		isActiveControl = true;
		holdForShoot = false;
		shot = false;
	}

	public void Deactivate() {
		isActiveControl = false;
	}
}
