﻿using UnityEngine;

public class Scores : MonoBehaviour {

	public static Scores instance { get; private set; }

	public  static int totalScores { get; private set; }
	public static int record { get; private set; }
	public static bool newHighScore { get; private set; }

	private const string PREF_KEY_RECORD = "SCORES_RECORD";

	private void Awake() {
		CreateSingleton();
		Load();
	}

	private void CreateSingleton() {
		instance = this;
	}

	public void Add(int count) {
		totalScores += count;
		ShowScores(count);
		if (totalScores > record) {
			newHighScore = true;
			record = totalScores;
			Save();
		}
	}

	private void ShowScores(int count) {
		if (NewScoresPanel.isInitialized)
			NewScoresPanel.instance.Show(count);
	}

	public void Remove(int count) {
		totalScores = Mathf.Max(totalScores - count, 0);
	}

	public void Reset() {
		totalScores = 0;
		newHighScore = false;
	}

	private void Load() {
		if (!PlayerPrefs.HasKey(PREF_KEY_RECORD))
			PlayerPrefs.SetInt(PREF_KEY_RECORD, 0);
		record = PlayerPrefs.GetInt(PREF_KEY_RECORD);
	}

	private void Save() {
		PlayerPrefs.SetInt(PREF_KEY_RECORD, record);
	}

	[ContextMenu("Clean record info")]
	private void CleanInfo() {
		PlayerPrefs.DeleteKey(PREF_KEY_RECORD);
		Debug.Log("Info about record was cleaned");
	}
}
