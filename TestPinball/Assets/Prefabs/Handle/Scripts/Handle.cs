﻿using UnityEngine;

public class Handle : MonoBehaviour {

	public float restPosition = 0f;
	public float pressetPosition = 45f;
	public float hitStrength = 10000f;
	public float handleDamper = 150f;

	private HingeJoint hinge;

	private enum Side {
		Right,
		Left
	}

	public Transform pivot;
	[Space(6)]
	[SerializeField]
	private Side side;
	public float maxAngle = 25f;
	public float speed = 1f;

	private Vector3 targetEuler;
	private Vector3 originalEulerAngles;

	private JointSpring jointSpring;

	private void Start() {
		Initialize();
	}

	private void Initialize() {
		jointSpring = new JointSpring();
		hinge = GetComponent<HingeJoint>();
		hinge.useSpring = true;
		originalEulerAngles = pivot.localEulerAngles;
		int clockwise = side == Side.Right ? -1 : 1;
		targetEuler = new Vector3(0f, 0f, originalEulerAngles.z + clockwise * maxAngle);
		if (targetEuler.z < 0)
			targetEuler.z = 360 + targetEuler.z;
	}

	private void FixedUpdate() {
		SetupSpring();

		if (GameManager.isManualMode)
			TryToRotateHandle();
		ApplySpring();
	}

	private void TryToRotateHandle() {

		if (side.Equals(Side.Right)) {
			if (CustomInput.holdRightHandle)
				HandlePressed();
			else
				HandleUnpressed();
		}
		else {
			if (CustomInput.holdLeftHandle)
				jointSpring.targetPosition = pressetPosition;
			else
				jointSpring.targetPosition = restPosition;
		}
		
	}

	private void SetupSpring() {
		jointSpring.spring = hitStrength;
		jointSpring.damper = handleDamper;
	}

	private void ApplySpring() {
		hinge.spring = jointSpring;
		hinge.useLimits = true;
	}

	public void HandlePressed() {
		SetupSpring();
		jointSpring.targetPosition = pressetPosition;
	}

	public void HandleUnpressed() {
		jointSpring.targetPosition = restPosition;
	}

}
