﻿using UnityEngine;

public class HandleAI : MonoBehaviour {

	[Range(0f, 100f)]
	public float chanceToHandle = 40;

	private Handle handle;
	private float timer;

	private void Awake() {
		Initialize();
	}

	private void Initialize() {
		handle = GetComponentInParent<Handle>();
	}

	private void OnTriggerEnter(Collider other) {
		Ball ball = other.GetComponentInParent<Ball>();
		if (ball != null)
			WorkHandle();
	}

	private void WorkHandle() {
		if (DoHandle())
			timer = 1f;
	}

	private bool DoHandle() {
		float rChance = Random.Range(0f, 100f);
		return rChance < chanceToHandle;
	}

	private void FixedUpdate() {
		if (!GameManager.isManualMode)
			LiveHandleAI();
	}

	private void LiveHandleAI() {
		if (timer > 0) {
			timer -= Time.fixedDeltaTime;
			handle.HandlePressed();
		}
		else {
			handle.HandleUnpressed();
		}
	}
}
